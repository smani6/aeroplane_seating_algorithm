import os
import config
from Manager import SeatAllocationAlgorithm

if __name__ == "__main__":

    """
            Main method to run the program.
            Input arrary and max_seat retrieved from config
            and SeatAllocationAlgorithm class is instantiated
            and called to allocate seats in the aircraft
    """

    inp_list = config.inp
    max_seat = config.max_seat
    mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
    seat_map = mgr_obj.allocate_seats()
    final_seat_map = mgr_obj.assign_seat_values(seat_map)
    mgr_obj.print_plane_seat_map(final_seat_map)