import pytest
from _pytest.monkeypatch import monkeypatch
from Manager import SeatAllocationAlgorithm


@pytest.fixture
def mgr_obj():
    inp_list = [[1, 2], [2, 3]]
    max_seat = 10
    mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
    return mgr_obj


class TestManager(object):

    @pytest.mark.test_allocate_seats_success1
    def test_allocate_seats1(self):
        inp_list = [[1, 2], [2, 3]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = mgr_obj.allocate_seats()
        print seat_map
        assert seat_map['window'][0]['coord'] == [1, 1]

    @pytest.mark.test_allocate_seats_success2
    def test_allocate_seats2(self):
        inp_list = [[1, 1], [1, 1]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = mgr_obj.allocate_seats()
        print seat_map
        assert seat_map['window'][0]['coord'] == [1, 1]
        assert seat_map['window'][1]['coord'] == [1, 2]

    @pytest.mark.test_allocate_seats_success3
    def test_allocate_seats3(self):
        inp_list = [[1, 2], [1, 2]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = mgr_obj.allocate_seats()
        print seat_map
        assert seat_map['asile'][0]['coord'] == [1, 2]
        assert seat_map['asile'][1]['coord'] == [1, 3]

    @pytest.mark.test_allocate_seats_failure1
    def test_allocate_seats_failure1(self):
        inp_list = [[1, 2], [1, 2]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = mgr_obj.allocate_seats()
        with pytest.raises(Exception) as ex:
            seat_map['center']
        assert ex.typename == "KeyError"

    @pytest.mark.test_allocate_seats_failure2
    def test_allocate_seats_failure2(self):
        inp_list = [[1, 1], [1, 1]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = mgr_obj.allocate_seats()
        print seat_map
        with pytest.raises(Exception) as ex:
            seat_map['asile'][0]
        assert ex.typename == "IndexError"

    @pytest.mark.test_allocate_seats_failure3
    def test_allocate_seats_failure3(self):
        inp_list = [[1, 1]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = mgr_obj.allocate_seats()
        with pytest.raises(Exception) as ex:
            seat_map['asile'][0] or seat_map['center'][0]
        assert ex.typename == "IndexError"

    @pytest.mark.test_assign_seat_values_success1
    def test_assign_seat_values1(self):

        inp_list = [[1, 2], [1, 2]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = {'middle': [], 'window': [{'coord': [1, 1]}, {'coord': [1, 4]}],
                    'asile': [{'coord': [1, 2]}, {'coord': [1, 3]}]}
        final_seat_map = mgr_obj.assign_seat_values(seat_map)
        assert final_seat_map['asile'][0]['seat_num'] == 1

    @pytest.mark.test_assign_seat_values_success2
    def test_assign_seat_values2(self):

        inp_list = [[2, 3], [2, 2]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = {'middle': [{'coord': [1, 2]}], 'window': [{'coord': [1, 1]}, {'coord': [1, 5]}],
                    'asile': [{'coord': [1, 3]}, {'coord': [1, 4]}]}
        final_seat_map = mgr_obj.assign_seat_values(seat_map)
        assert final_seat_map['middle'][0]['seat_num'] == 5

    @pytest.mark.test_assign_seat_values_failure1
    def test_assign_seat_values_failure1(self):

        inp_list = [[2, 3], [2, 2]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = {'middle': [], 'window': [{'coord': [1, 1]}, {'coord': [1, 5]}],
                    'asile': [{'coord': [1, 3]}, {'coord': [1, 4]}]}
        final_seat_map = mgr_obj.assign_seat_values(seat_map)
        with pytest.raises(Exception) as ex:
            assert final_seat_map['middle'][0] == 5

    @pytest.mark.test_assign_seat_values_failure2
    def test_assign_seat_values_failure2(self):

        inp_list = [[2, 3], [2, 2]]
        max_seat = 10
        mgr_obj = SeatAllocationAlgorithm(inp_list, max_seat)
        seat_map = {'middle': [], 'window': [{'coord': [1, 1]}, {'coord': [1, 5]}],
                    'asile': [{'coord': [1, 3]}, {'coord': [1, 4]}]}
        final_seat_map = mgr_obj.assign_seat_values(seat_map)
        with pytest.raises(Exception) as ex:
            assert final_seat_map['asile'][2]['seat_num'] == 3
