import numpy


class SeatAllocationAlgorithm(object):

    def __init__(self, inp_list, max_seat):
        self.inp_list = inp_list
        self.max_seat = max_seat
        self.last_seat = 0
        self.max_x = 0
        self.col_count = 0

    def allocate_seats(self):
        """
            Method to allocate asile, window and center seats
            in the seating order.
            Will process each_row based on the inner input array
            length (till each input column) and assign seats to it
            based on certain conditions.

        """
        try:
            inp_list = self.inp_list
            self.max_x, self.col_count = self.__get_max_row_count(inp_list)
            max_seat = self.max_seat
            seat_map = {}
            seat_map['asile'] = []
            seat_map['window'] = []
            seat_map['middle'] = []
            for row in range(1, self.max_x + 1):  # For each row processiing
                prev_coord_y = 0
                for inp_pos in range(1, len(inp_list) + 1):  # For each input in input List
                    coord_x = inp_list[inp_pos - 1][0]
                    coord_y = inp_list[inp_pos - 1][1]
                    if (coord_x) >= row:
                        for inp in range(1, coord_y + 1):  # For each position in input
                            if inp_pos == 1:
                                if inp == 1 and inp_pos == 1:  # Window seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['window'].append(seat_dict)
                                # Special Case of Solo Window seat
                                elif inp == coord_y and self.col_count == coord_y:
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['window'].append(seat_dict)
                                elif inp == coord_y:  # Asile Seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['asile'].append(seat_dict)
                                else:  # Middle Seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['middle'].append(seat_dict)
                            elif inp_pos == (len(inp_list)):
                                if inp == coord_y:  # WIndow seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['window'].append(seat_dict)
                                elif inp == 1:  # Asile seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['asile'].append(seat_dict)
                                else:  # Middle Seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['middle'].append(seat_dict)
                            else:
                                if inp == 1:  # Asile seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['asile'].append(seat_dict)
                                elif inp == coord_y:  # Asile seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['asile'].append(seat_dict)
                                else:  # Middle Seat
                                    seat_dict = {}
                                    seat_dict['coord'] = [row, inp + prev_coord_y]
                                    seat_map['middle'].append(seat_dict)
                    prev_coord_y += coord_y

            return seat_map

        except Exception as e:
            print e

    def assign_seat_values(self, seat_map):
        """
            Method to assing seat values to the
            seat name
        """
        try:
            final_seat_map = {}
            final_seat_map['asile'] = []
            final_seat_map['window'] = []
            final_seat_map['middle'] = []

            # Allocating Seat number for Asile
            asile_seat_list = seat_map['asile']
            for asile_seat_pos in range(0, len(asile_seat_list)):
                asile_seat_dict = asile_seat_list[asile_seat_pos]
                if self.max_seat == self.last_seat:
                    break
                self.last_seat += 1
                asile_seat_dict['seat_num'] = self.last_seat
                final_seat_map['asile'].append(asile_seat_dict)
            # Allocating Seat number for Window
            window_seat_list = seat_map['window']
            for window_seat_pos in range(0, len(window_seat_list)):
                window_seat_dict = window_seat_list[window_seat_pos]
                if self.max_seat == self.last_seat:
                    break
                self.last_seat += 1
                window_seat_dict['seat_num'] = self.last_seat
                final_seat_map['window'].append(window_seat_dict)
            # Allocating Seat number for Middle
            middle_seat_list = seat_map['middle']
            for middle_seat_pos in range(0, len(middle_seat_list)):
                middle_seat_dict = middle_seat_list[middle_seat_pos]
                if self.max_seat == self.last_seat:
                    break
                self.last_seat += 1
                middle_seat_dict['seat_num'] = self.last_seat
                final_seat_map['middle'].append(middle_seat_dict)
            return final_seat_map

        except Exception as e:
            print e

    def __get_max_row_count(self, inp_list):
        """
            Method to return max_row count and
            sum of col_val
        """
        row_list = [inp[0] for inp in inp_list]
        col_list = [inp[1] for inp in inp_list]
        return max(row_list), sum(col_list)

    def print_plane_seat_map(self, seat_map_dict):
        """
            Method to print the output on the console,
            showing allocated seats in the aircraft
        """

        try:
            arrary = numpy.zeros((self.max_x, self.col_count))

            aero_seats = seat_map_dict['asile'] + seat_map_dict['window'] + seat_map_dict['middle']
            for each_seat in aero_seats:
                seat_num = each_seat['seat_num']
                cord = each_seat['coord']
                arrary[cord[0] - 1][cord[1] - 1] = seat_num

            seat_matrix = numpy.asmatrix(arrary)
            for x in range(0, self.max_x):
                print "\n"
                row_print = ""
                for y in range(0, self.col_count):
                    space_num = 0
                    for inp_elt in self.inp_list:
                        space_num += inp_elt[1]
                        if space_num == y:
                            row_print += "\t"
                    row_print += "\t"
                    if seat_matrix[x, y] != 0:
                        row_print += str(int(seat_matrix[x, y]))
                print row_print

        except Exception as e:
            print e
